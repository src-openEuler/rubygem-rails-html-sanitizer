%global gem_name rails-html-sanitizer

Name:       rubygem-%{gem_name}
Version:    1.4.3
Release:    1
Summary:    This gem is responsible to sanitize HTML fragments in Rails applications
License:    MIT
URL:        https://github.com/rails/rails-html-sanitizer
Source0:    https://rubygems.org/gems/%{gem_name}-%{version}.gem
# https://github.com/rails/rails-html-sanitizer/pull/143
# libxml2 2.10.x changes incorrectly opened comments parsing
Patch0:	    %{name}-1.4.3-tests-libxml2-2_10_0-parsing-comments-change.patch
BuildRequires:  ruby(release)
BuildRequires:  rubygems-devel
BuildRequires:  ruby
BuildRequires:  rubygem(loofah)
BuildRequires:  rubygem(minitest)
BuildRequires:  rubygem(rails-dom-testing)
BuildArch:      noarch

%description
HTML sanitization for Rails applications.


%package    doc
Summary:    Documentation for %{name}
Requires:   %{name} = %{version}-%{release}
BuildArch:  noarch

%description doc
Documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version}
%patch0 -p1

%build
gem build ../%{gem_name}-%{version}.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

%check
pushd .%{gem_instdir}
ruby -Ilib -e 'Dir.glob "./test/**/*_test.rb", &method(:require)'
popd

%files
%dir %{gem_instdir}
%license %{gem_instdir}/MIT-LICENSE
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/CHANGELOG.md
%doc %{gem_instdir}/README.md
%{gem_instdir}/test

%changelog
* Mon Aug 14 2023 liqiuyu <liqiuyu@kylinos.cn> - 1.4.3-1
- Upgrade to 1.4.3

* Thu Jul 14 2022 baizhonggui <baizhonggui@h-partners.com> - 1.4.2-2
- Fix test failures

* Wed May 4 2022 wangkerong <wangkerong@h-partners.com> - 1.4.2-1
- Upgrade to 1.4.2

* Tue Aug 25 2020 huangyangke <huangyangke@huawei.com> - 1.0.4-1
- package init